import pyslurry.fluid as fluid
import unittest


class FluidsTestCase(unittest.TestCase):

    def test_water_rho(self):
        water = fluid.Water()
        rho = 1000
        self.assertEqual(water.rho, rho)

    def test_water_mu(self):
        water = fluid.Water()
        mu = 1.006E-3
        self.assertEqual(water.mu, mu)

if __name__ == '__main__':
    unittest.main()
