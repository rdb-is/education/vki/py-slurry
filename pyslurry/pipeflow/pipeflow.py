""" This module contains the class that defines pipe flow cases (e.g. Horizontal
pipe, inclined pipe, ecc... """

from abc import abstractmethod, ABCMeta


class PipeFlow():
    """ Abstract class that represents a pipe flow case
    """
    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def D(self):
        """ Diameter of the pipe """
        pass

    @property
    @abstractmethod
    def L(self):
        """ Length of the pipe """
        pass

    @property
    @abstractmethod
    def U(self):
        """ Mean velocity of the fluid inside the pipe """
        pass


class HorizontalPipe(object):
    """ This class represents an horizontal pipe case with gravity

    Attributes:

    """
    _g = 9.81

    def __init__(self, D, L, U, eps=0):
        """ Constructor

        Args:
            fluid(pyslurry.fluid.Fluid): Instance of the fluid flowing inside
                the horizontal pipe
            D(float): Diameter of the pipe                      [m]
            L(float): Length of the pipe                        [m]
            U(float): Mean velocity of the fluid flowing inside the horizontal
                pipe                                            [m/s]
            eps(float): Roughness of the pipe                   [?]
        """

        self.D = D
        self.L = L
        self.U = U
        self.eps = eps

    @property
    def g(self):
        """ Gravity field exherting influence on the flow orthogonally to the
        length of the pipe"""

        return self._g

    @g.setter
    def g(self, value):
        self._g = value

    def Re(self, fluid):
        """ This method computes the Reynolds number

        Args:
            fluid(fluid.Fluid): The fluid for which the Reynolds number
                must be computed

        """

        return(fluid.rho * self.U * self.D / fluid.mu)
