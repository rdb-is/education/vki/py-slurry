""" This module contains all the pressure correlations related to slurry flows
in pipes.  """

from __future__ import print_function
from __future__ import division

import pyslurry.drag

from abc import ABCMeta, abstractmethod
from numpy import *
from pyslurry.fluid import Water
from scipy.optimize import fsolve


class PressureCorrelation():
    """ Abstract Class that represents a pressure drop correlation.
    """
    __metaclass__ = ABCMeta

    def __init__(self, fluid, geometry):
        """ Constructor

        Args:
            case(pyslurry.pipeflow.PipeFlow): The case for which this
                correlation should  be calculated
        """
        self.fluid = fluid
        self.pipe = geometry
        self.Re = geometry.Re(fluid)
        self.regime = self.delineate_flow_regime()

    @abstractmethod
    def delineate_flow_regime(self):
        """ Method to delineate the regime of the flow:

        Returns:
            flow_regime(list): A list containing the flow regimes
                (see implementations)

        """

    @abstractmethod
    def dp(self):
        """ This method returns the pressure drop over length computed by the
        intended correlation
        """
        pass

    @abstractmethod
    def f(self, *args, **kwargs):
        """ This method returns the Darcy friction factor. """
        pass

    def to_darcy(self, f_f):
        r""" Converts the Fanning friction factor:

        .. math:: f_\text{Fanning}\frac{\Delta P}{L}\frac{D}{2 \rho U^2}

        to the Darcy friction factor:

        .. math:: f_\text{Darcy} = 4 f_\text{Fanning}

        Args:
            f_f(float): Fanning friction factor

        Returns:
            f_d(float): Darcy friction factor

        """

        return 4*f_f

    def to_fanning(self, f_d):
        r""" Converts the Darcy friction factor:

        .. math:: f_\text{Darcy} = \frac{\Delta P}{L} \frac{2D}{\rho U^2}

        to the Fanning friction factor:

        .. math:: f_\text{Fanning} = \frac{1}{4} f_\text{Darcy}

        Args:
            f_d(float): Darcy friction factor

        Returns:
            f_f(float): Fanning frictin factor

        """

        return f_d/4


class Poiseuille(PressureCorrelation):
    """ Class that implements the Poiseuille flow model

    Attributes:
        pipe(pipeflow.pipeflow.HorizontalPipe): The case which the pressure drop
            must be estimated for
        fluid(fluid.Fluid): The fluid flowing in the pipe
        Re(float): The Reynolds number of the case
        regime(string): The regime of the flow ('laminar')
    """
    def __init__(self, fluid, horizontal_pipe):
        super().__init__(fluid, horizontal_pipe)

    def delineate_flow_regime(self):
        """ This method checks for the laminarity of the flow. If not raises
            a ValueError.

        Raises:
            ValueError: A ValueError is raised if the fluid is not laminar
        """

        Re = self.Re

        if Re > 1500:
            raise ValueError("The Reynold's number is too high. Poiseuille"
                             "model not applicable")
        return 'laminar'

    def dp(self):
        mu = self.fluid.mu
        U = self.pipe.U
        D = self.pipe.D

        return 32*mu*U/D**2

    def f(self):
        D = self.pipe.D
        rho = self.fluid.rho
        U = self.pipe.U

        return self.dp()*2*D/rho/U**2


class NonDevPoiseuille(Poiseuille):
    r""" This class implements the model for non-developed laminar flow


    .. math:: \Delta \tilde{P} = 13.74 \sqrt{\tilde{L}} +
        \frac{1.25 + 64 \tilde{L} - 13.74\sqrt{\tilde{L}}}
        {1 + 0.00021 \left( \tilde{L}\right)^{-2}}

    where :math:`\tilde{L} = \frac{L}{D Re_D}` is the normalized length of
    the pipe and :math:`\Delta \tilde{P} = 2 \frac{\Delta P}{\rho U}` the
    normalized pressure drop

    """

    def __init__(self, fluid,  horizontal_pipe):
        super().__init__(fluid, horizontal_pipe)

    def dp(self):

        L = self.pipe.L
        Re = self.Re
        D = self.pipe.D
        U = self.pipe.U
        rho = self.fluid.rho

        L_ = L/Re/D
        DP_ = 13.74*sqrt(L_) + (1.25 + 64*L_ - 13.74*sqrt(L_))/(1 + 0.0021 *
                                                                L_ ** (-2))

        return DP_/2*rho*U


class Turbulent(PressureCorrelation):
    r""" Pressure correlation for turbulent pure flow. Uses the Colebrook
    relation for the friction factor:

    .. math:: \frac{1}{\sqrt{f}} = -2.0 \log \left( \frac{\frac{\epsilon}{D}
            }{3.7} + \frac{2.51}{Re\sqrt{f}} \right)

    """

    def __init__(self, fluid,  horizontal_pipe):
        """ Constructor

        Args:
            horizontal_pipe(pipeflow.pipeflow.HorizontalPipe):
                The instance of the case for which the pressure drop has to be
                calculated
        """
        super().__init__(fluid, horizontal_pipe)

    def _colebrook(self, f, eps, D, Re):
        """ Function that represents the Colebrook correlation for friction
        factor """

        return -2.0*log10(eps/3.71/D + 2.51/Re/sqrt(f)) - 1/sqrt(f)

    def f(self, x0=0.01):
        """ Function that solves for Colebrook's Darcy friction factor equation
        using the Newton-Rhapson method

        Args:
            x0: Initial Guess for the solution (default to 0.01)

        Returns:
            f(float): Darcy friction factor

        """
        eps = self.pipe.eps
        D = self.pipe.D
        Re = self.Re

        return fsolve(self._colebrook, x0, args=(eps, D, Re), xtol=1E-6)[0]

    def dp(self):
        f = self.f()
        D = self.pipe.D
        rho = self.fluid.rho
        U = self.pipe.U

        return f/D * rho*U**2/2

    def delineate_flow_regime(self):
        """ This method checks for the laminarity of the flow. If not raises
            a ValueError.

        Returns:
            regime(string): The flow regime

        Raises:
            ValueError: A ValueError is raised if the fluid is not turbulent
        """
        Re = self.Re
        if Re < 1500:
            raise ValueError("The Reynold's number is too low. Turbulent "
                             "model not applicable")
        return 'turbulent'


class TurianYuan1977(Turbulent):
    r"""
    Based on:

    **Turian and Yuan**
    "Flow of Slurries in Pipelines", *AlChE Journal (Vol. 23, No. 3), 1977*

    In that paper the pressure drop is given as difference from the friction
    factor of pure water (f-fw).

    .. math:: f-f_w = K C^{\alpha} f_w^{\beta} C_D^{\gamma} \left[
                \frac{v^2}{D g (s-1)} \right ]

    Attributes:
        regime(str): The slurry flow regime ['stationary bed', 'saltation',
            'heterogeneous', 'homogeneous']
        pipe(pipeflow.pipeflow.HorizontalPipe): The Horizontal Pipe case which
            the pressure drop must be estimated for.
        fluid(fluid.Slurry): The slurry fluid object
    """

    def __init__(self, fluid, horizontal_pipe):
        """ Constructor

        Args:
            fluid(pyslurry.fluid.Slurry): The slurry fluid for which the
                pressure drop has to be calculated
            horizontal_pipe(pipeflow.HorizontalPipe): The instance of the case
                for which the pressure drop has to be calculated
        """

        self.fluid = fluid
        self.pipe = horizontal_pipe
        self.Re = self.pipe.Re(self.fluid)

        # Parameter used to estimate the CD of the particles
        # see pag. 240

        self.regime = self.delineate_flow_regime()

        # Coefficients of the correlation calculated in accordance with the
        # flow regime
        self._params()

    def _params(self):
        """ Sets the parameters of the correlation accordingly with the flow
        regime """

        regimes = {
            'stationary bed': self._stationary_bed_params(),
            'saltation': self._saltation_params(),
            'heterogeneous': self._heterogeneous_params(),
            'homogeneous': self._homogeneous_params()
            }

        return regimes.get(self.regime)

    def _stationary_bed_params(self):

        self._K = 0.4036
        self._alpha = 0.7389
        self._beta = 0.7717
        self._gamma = -0.4054
        self._delta = -1.096

    def _saltation_params(self):

        self._K = 0.9857
        self._alpha = 1.018
        self._beta = 1.046
        self._gamma = -0.4213
        self._delta = -1.354

    def _heterogeneous_params(self):

        self._K = 0.5513
        self._alpha = 0.8687
        self._beta = 1.200
        self._gamma = -0.1677
        self._delta = -0.6938

    def _homogeneous_params(self):

        self._K = 0.8444
        self._alpha = 0.5024
        self._beta = 1.428
        self._gamma = 0.1516
        self._delta = -0.3531

    def _Rab(self, kappa, alpha, beta, gamma):
        """ This method represents the regime delineation functions in the
        form of:

        .. math:: \frac{U^2}{K C^{\alpha} f_w^{\beta} C_D^{\gamma} Dg(s-1)}

        """

        U = self.pipe.U
        D = self.pipe.D
        g = self.pipe.g
        C = self.fluid.C
        s = self.fluid.s

        fw = self._fw()
        CD = self.CD()

        return (U**2)/(kappa*C**(alpha) * fw**(beta) * CD**(gamma) * D*g*(s-1))

    def _R01(self):
        """ Regime delineation function (pag. 237) from stat. bed to
        saltation """
        K = 31.93
        alpha = 1.083
        beta = 1.064
        gamma = -0.06160
        return self._Rab(K, alpha, beta, gamma)

    def _R12(self):
        """ Regime delineation function (pag. 237) from saltation to
        heterogeneous """

        K = 2.411
        alpha = 0.2263
        beta = -0.2334
        gamma = -0.3840
        return self._Rab(K, alpha, beta, gamma)

    def _R23(self):
        """ Regime delineation function (pag. 237) from heterogeneous to
        homogeneous """

        K = 0.2859
        alpha = 1.075
        beta = -0.6700
        gamma = -0.9375
        return self._Rab(K, alpha, beta, gamma)

    def _R13(self):
        """ Regime delineation function (pag. 239) from saltation to
        homogeneous """

        K = 1.167
        alpha = 0.5153
        beta = -0.3820
        gamma = -0.5724
        return self._Rab(K, alpha, beta, gamma)

    def _R02(self):
        """ Regime delineation function (pag. 239) from stat. bed to
        heterogeneous """

        K = 0.4608
        alpha = -0.3225
        beta = -1.065
        gamma = -0.5906
        return self._Rab(K, alpha, beta, gamma)

    def _R03(self):
        """ Regime delineation function (pag. 239) from stat. bed to
        homogeneous """

        K = 0.3703
        alpha = 0.3183
        beta = -0.8837
        gamma = -0.7496
        return self._Rab(K, alpha, beta, gamma)

    def _fw(self):
        """ This method computes the Fanning friction factor for water """

        water = Water()
        turbulent_pipe = Turbulent(water, self.pipe)

        return self.to_fanning(turbulent_pipe.f())

    def delineate_flow_regime(self):
        """ This method returns a string telling which is the flow regime

        Returns:
            flow_regime(str): the flow regime of the slurry in the pipe
        """

        # Slurry Regimes
        regimes = {
            '0': 'stationary bed',
            '1': 'saltation',
            '2': 'heterogeneous',
            '3': 'homogeneous'
        }

        # Functions needed to delineate the flow regime
        R01_1 = self._R01() - 1
        R12_1 = self._R12() - 1
        R23_1 = self._R23() - 1

        # Checks (see pag. 240 of the paper)
        # TODO: Make it more pythonic or clearer ='(

        if R01_1 < 0 and R12_1 < 0 and R23_1 < 0:
            flow_regime = regimes['0']

        elif R01_1 > 0 and R12_1 < 0 and R23_1 < 0:
            flow_regime = regimes['1']

        elif R01_1 > 0 and R12_1 > 0 and R23_1 < 0:
            flow_regime = regimes['2']

        elif R01_1 > 0 and R12_1 > 0 and R23_1 > 0:
            flow_regime = regimes['3']

        # Odd configuration A
        elif R01_1 < 0 and R12_1 < 0 and R23_1 > 0:
            R03_1 = self._R03() - 1

            if R03_1 < 0:
                flow_regime = regimes['0']

            else:
                flow_regime = regimes['3']

        # Odd configuration B
        elif R01_1 < 0 and R12_1 > 0 and R23_1 > 0:
            R03_1 = self._R03() - 1

            if R03_1 < 0:
                flow_regime = regimes['0']
            else:
                flow_regime = regimes['3']

        # Odd configuration C
        elif R01_1 < 0 and R12_1 > 0 and R23_1 < 0:
            R02_1 = self._R02() - 1

            if R02_1 < 0:
                flow_regime = regimes['0']
            else:
                flow_regime = regimes['2']

        # Odd configuration D
        elif R01_1 > 0 and R12_1 < 0 and R23_1 > 0:
            R13_1 = self._R13() - 1

            if R13_1 < 0:
                flow_regime = regimes['1']
            else:
                flow_regime = regimes['3']

        return flow_regime

    def f(self):
        fw = self._fw()
        g = self.pipe.g
        s = self.fluid.s
        C = self.fluid.C
        U = self.pipe.U
        D = self.pipe.U
        CD = self.CD()

        v2_Dgs_1 = U**2/D/g/(s - 1)

        K = self._K
        alpha = self._alpha
        beta = self._beta
        gamma = self._gamma
        delta = self._delta

        return self.to_darcy(fw + K*C**alpha * fw**beta * CD**gamma *
                             v2_Dgs_1**delta)

    def CD(self):
        drag_model = getattr(pyslurry.drag,
                             'TurianYuanDragCorrelation')

        # TODO: Here I'm currently using the pipe mean velocity for the
        # calculation of the CD but the slip velocity must be used (v_s - U)
        drag_instance = drag_model(self.fluid, self.pipe.U)

        return drag_instance.CD()


class DurandCondoliosGibert(PressureCorrelation):
    r""" This class implements the pressure drop model provided by Durand and
    Condolios in the paper:

        **Durand, R. and Condolios, E.**
        "Etude experimentale du refoulement des materiaux en conduites en
        particulier des produites de dragage et des schlams", Deuxiemes Journees
        de l'Hydraulique, 1952

    together with the modification suggested in:

        **Girbert, R.**
        "Transport hydraulique et refoulement des mixtures en conduites",
        Annales des Ponts et Chausees, 1960

    check also:
        **Sape A. Miedema**
        "An Overview of theories describing head losses in slurry transport, a
        tribute to early researchers"

    .. math:: \Delta P = \Delta P_w \left( 1+\Phi C \right)

    """

    def __init__(self, fluid, pipe, drag='TurianYuanDragCorrelation'):
        """ Constructor

        Args:
            fluid(pyslurry.fluid.Slurry): The slurry fluid for which the
                pressure drop has to be calculated
            horizontal_pipe(pipeflow.HorizontalPipe): The instance of the case
                for which the pressure drop has to be calculated
            drag(string): The name of the drag model to be used (check
                pyslurry.drag for more info)
        """

        super().__init__(fluid, pipe)
        self._params()
        self.drag_model = getattr(pyslurry.drag, drag)(self.fluid)

    def _params(self):
        """ Sets the correlation parameters """

        self._K = 80
        self._n = -3/2

    def dp(self):
        dp_w = self._dpw()
        Phi = self._Phi()
        C = self.fluid.C

        return dp_w*(1 + Phi*C)

    def f(self):
        D = self.pipe.D
        rho = self.fluid.rho
        U = self.pipe.U

        return self.dp()*2*D/rho/U**2

    def Frp(self):
        """ The Froude number for the particle"""

        vt = self.vt()
        g = self.pipe.g
        d = self.fluid.d

        return vt/sqrt(g*d)

    def Frfl(self):
        """ The Froude number for the fluid """
        U = self.pipe.U
        D = self.pipe.D
        g = self.pipe.g
        s = self.fluid.s

        return U/sqrt(g*D*(s-1))

    def vt(self):

        """ The terminal settling velocity of particles (also limit velocity,
        settling velocity ecc... """

        g = self.pipe.g
        s = self.fluid.s
        d = self.fluid.d
        CD = self.CD()

        return sqrt(4/3 * (s-1)/CD * g * d)

    def CD(self):

        return self.drag_model.CD()

    def _Psi(self):
        Frfl2 = self.Frfl()**2
        Frp = self.Frp()

        return Frfl2*Frp**(-1)

    def _Phi(self):
        K = self._K
        n = self._n
        Psi = self._Psi()

        return K*Psi**(n)

    def _dpw(self):
        """ Turbulent pressure drop for pure water """

        turbulent_case = Turbulent(self.fluid, self.pipe)

        return turbulent_case.dp()
